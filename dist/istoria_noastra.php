<?php

include '_includes/header.php';

?>

  <div class="row">
    <div class="background-image">
      <img src="bbf891e9650013a5835b12b4f10455ad.png" alt="" class="individual-background">
      <div class="individual-text">
        <h1>Istoria noastra</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare dui
          in
          tellus commodo, ut consequat elit pulvinar. Cras sit amet mi tincidunt, mattis purus sit
          amet,
          tempus lacus.
        </p>
        <h3>Prietenii nostrii sunt natura si materialele naturale</h3>
      </div>
    </div>
  </div>
  <div class="row about-text">
    <div class="col-6 text-section">
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima laudantium nostrum eligendi consectetur.
        Doloribus, qui tempore. Sit perspiciatis ut explicabo quaerat expedita cupiditate nemo fuga, aliquam praesentium
        libero eligendi consequuntur.</p>
    </div>
    <div class="col-6">
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima laudantium nostrum eligendi consectetur.
        Doloribus, qui tempore. Sit perspiciatis ut explicabo quaerat expedita cupiditate nemo fuga, aliquam praesentium
        libero eligendi consequuntur.</p>
    </div>
  </div>
  <div class="row background-image">
    <img src="bbf891e9650013a5835b12b4f10455ad.png" alt="" class="individual-background">
  </div>
  <div class="row about-text">
    <div class="col text-section">
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Minima laudantium nostrum eligendi consectetur.
        Doloribus, qui tempore. Sit perspiciatis ut explicabo quaerat expedita cupiditate nemo fuga, aliquam praesentium
        libero eligendi consequuntur.</p>
    </div>
  </div>

 <?php

 include '_includes/footer.php';

 ?>