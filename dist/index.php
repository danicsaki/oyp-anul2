<?php

include '_includes/header.php';

?>

  <div class="outer-wrapper">
      <div class="wrapper">
          <a href="despre_noi.php">
              <div class="slide one">
                  <div class="slide-background">
                      <img src="3346fb7df548990e817ab6c330b038e5.png" alt="">
                  </div>
                  <div class="slide-text">
                      <h1>Despre noi</h1>
                      <p class="showme">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare dui
                          in
                          tellus commodo, ut consequat elit pulvinar. Cras sit amet mi tincidunt, mattis purus sit
                          amet,
                          tempus lacus.
                      </p>
                      <h4>Prietenii nostrii sunt natura si materialele naturale</h4>
                  </div>
              </div>
          </a>
          <a href="produse.php">
              <div class="slide two">
                  <div class="slide-background">
                      <img src="ed4e6000f172ad02a9a885f34943b7c0.png " alt="">
                  </div>
                  <div class="slide-text">
                      <h1>Produse</h1>
                      <p class="showme">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare dui
                          in
                          tellus commodo, ut consequat elit pulvinar. Cras sit amet mi tincidunt, mattis purus sit
                          amet,
                          tempus lacus.
                      </p>
                      <h2>Lorem ipsum</h2>
                  </div>
              </div>
          </a>
          <a href="istoria_noastra.php">
              <div class="slide three">
                  <div class="slide-background">
                      <img src="bbf891e9650013a5835b12b4f10455ad.png" alt="">
                  </div>
                  <div class="slide-text">
                      <h1>Istoria noastra</h1>
                      <p class="showme">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare dui
                          in
                          tellus commodo, ut consequat elit pulvinar. Cras sit amet mi tincidunt, mattis purus sit
                          amet,
                          tempus lacus.
                      </p>
                      <h2>Lorem ipsum</h2>
                  </div>
              </div>
          </a>
          <a href="#">
              <div class="slide four">
                  <div class="slide-background">
                      <img src="76f798e746a2daf39cba28024be0dda6.png" alt="">
                  </div>
                  <div class="slide-text">
                      <h1>Lorem</h1>
                      <p class="showme">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare dui
                          in
                          tellus commodo, ut consequat elit pulvinar. Cras sit amet mi tincidunt, mattis purus sit
                          amet,
                          tempus lacus.
                      </p>
                      <h2>Lorem ipsum</h2>
                  </div>
              </div>
          </a>
          <a href="contact.php">
              <div class="slide five">
                  <div class="slide-background">
                      <img src="ee4730959a320350f621024907fec68d.png" alt="">
                  </div>
                  <div class="slide-text">
                      <h1>Contact</h1>
                      <p class="showme">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare dui
                          in
                          tellus commodo, ut consequat elit pulvinar. Cras sit amet mi tincidunt, mattis purus sit
                          amet,
                          tempus lacus.
                      </p>
                      <h2>Lorem ipsum</h2>
                  </div>
              </div>
          </a>
      </div>
  </div>

  <?php

  include '_includes/footer.php';

  ?>