<?php

include '_includes/header.php';

?>

  <div class="row normal-content">
    <div class="col-6 contact-div background-image">
      <img src="ee4730959a320350f621024907fec68d.png" alt="" class="individual-background">
    </div>
    <div class="col-6 text-section">
      <h1 class="display-4 contact-title">Contact</h1>
      <div class="contact-content">
        <h4>Email: kelevtrans@yahoo.com</h4>
        <h4>Telefon: +04 0744 204 346</h4>
        <h4>Adresa: com. Pănet / Mezőpanit , str. Principală nr. 3, jud. Mureş</h4>
      </div>
      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2744.0325119592835!2d24.47063721581049!3d46.54704366855691!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x474bc858ba2a14ab%3A0x8e04b6b2b8a4593a!2sStrada%20Principal%C4%83%203%2C%20P%C4%83net!5e0!3m2!1sen!2sro!4v1573314314504!5m2!1sen!2sro"
        width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" class="contact-map"></iframe>
    </div>
  </div>

  <div class="mobile-content">
    <div class="row contact-background background-image">
      <img src="ee4730959a320350f621024907fec68d.png" alt="">
    </div>
    <div class="row">
      <div class="col text-section">
        <h1 class="display-4 contact-title">Contact</h1>
        <div class="contact-content">
          <h4>Email: kelevtrans@yahoo.com</h4>
          <h4>Telefon: +04 0744 204 346</h4>
          <h4>Adresa: com. Pănet / Mezőpanit , str. Principală nr. 3, jud. Mureş</h4>
        </div>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2744.0325119592835!2d24.47063721581049!3d46.54704366855691!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x474bc858ba2a14ab%3A0x8e04b6b2b8a4593a!2sStrada%20Principal%C4%83%203%2C%20P%C4%83net!5e0!3m2!1sen!2sro!4v1573314314504!5m2!1sen!2sro"
          frameborder="0" style="border:0;" allowfullscreen="" class="contact-map"></iframe>
      </div>
    </div>
  </div>

<?php

include '_includes/footer.php';

?>