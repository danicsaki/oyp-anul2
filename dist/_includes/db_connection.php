<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "oyp";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// sql to create table
$sql = "CREATE TABLE products (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
product_name VARCHAR(30) NOT NULL,
product_price DECIMAL(30) NOT NULL,
product_image VARCHAR(50) NOT NULL UNIQUE,
product_details TEXT
)";

if ($conn->query($sql) === TRUE) {
    echo "<script>console.log('Table products created successfully');</script>";
} else {
    echo "<script>console.log('Error creating table');</script>";
}


// function createProduct(){
//     $query = "INSERT INTO products (product_name, product_price, product_image, product_details)
// VALUES ('Adela', '30', 'adela_small.jpg', '<ul>
//   <li>Caracteristică: Ţesătură 100 % bumbac. Ţesătură mai groasă, aspect neted.</li>
//   <li>Utilizare: Cămăşi de iarnă, bluze de iarnă.</li>
//   <li>Lăţime: 135-140 cm.</li>
//   <li>Greutate: 249 g/m2.</li>
//   <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
// </ul>')";

// if ($conn->query($query) === TRUE) {
//     echo "New record created successfully";
//     } else {
//     echo "Error: " . $query . "<br>" . $conn->error;
//     }
// }

// createProduct();

// createProduct('Adela', '30', 'adela_small.jpg', '<ul>
// <li>Caracteristică: Ţesătură 100 % bumbac. Ţesătură mai groasă, aspect neted.</li>
// <li>Utilizare: Cămăşi de iarnă, bluze de iarnă.</li>
// <li>Lăţime: 135-140 cm.</li>
// <li>Greutate: 249 g/m2.</li>
// <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
// </ul>')

    $query = "INSERT INTO products (product_name, product_price, product_image, product_details)
     VALUES ('Adela', '30', 'adela_small.jpg', '<ul>
       <li>Caracteristică: Ţesătură 100 % bumbac. Ţesătură mai groasă, aspect neted.</li>
       <li>Utilizare: Cămăşi de iarnă, bluze de iarnă.</li>
       <li>Lăţime: 135-140 cm.</li>
       <li>Greutate: 249 g/m2.</li>
       <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
     </ul>'), ('Amalia', '30', 'amalia_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Uşor creponat.</li>
  <li>Utilizare: Bluze e vară.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 107 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Art 1(a)', '30', 'art11_small.jpg', '<ul>
  <li>Caracteristică: 70% in + 30% bumbac.</li>
  <li>Utilizare: Perdele rustice, feţe de masă, prosoape de bucătărie.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 266 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată.</li>
</ul>'), ('Art 1(b)','30','art12_small.jpg','<ul>
  <li>Caracteristică: 30% in + 70% bumbac.</li>
  <li>Utilizare: Perdele rustice, feţe de masă, prosoape de bucătărie.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 266 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată.</li>
</ul>'), ('Art 2', '30', 'art2_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Feţe de masă, prosoape de bucătărie.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 266 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Bianca', '30', 'bianca_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Tuşeu: moale.</li>
  <li>Utilizare: Cămăşi bărbaţi, fuste rustice.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 202 sg/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Irina', '30', 'irina_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Uşor creponată.</li>
  <li>Utilizare: Bluze de vară.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 100 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Marta', '30', 'marta_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Creponată.</li>
  <li>Utilizare: Rochii şi fuste stil rustic, bluze rustice de iarnă.</li>
  <li>Lăţime: 120 cm.</li>
  <li>Greutate: 178 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Sfion 34', '30', 'sifon34_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Tuşeu: moale.</li>
  <li>Utilizare: Bluze, cămăşi, fuste stil rustic.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 142 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Rips 34', '30', 'rips34_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Tuşeu: uşor rifelat (efect de boabe de grâu).</li>
  <li>Utilizare: Haine exterioare, bluze, cămăşi.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 172 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Rita 2', '30', 'rita2_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Tuşeu: uşor creponată.</li>
  <li>Utilizare: Bluze, rochii stil rustic.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 130 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Serena', '30', 'serena_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Bluze, fuste stil rustic.</li>
  <li>Lăţime: 110 cm.</li>
  <li>Greutate: 160 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Serena N', '30', 'serenaB_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Bluze, fuste stil rustic.</li>
  <li>Lăţime: 110 cm.</li>
  <li>Greutate: 160 g/m2.</li>
  <li>Observaţie: Comercializăm în formă vopsită (negru) la comandă.</li>
</ul>'), ('Tifon', '30', 'tifon_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Uşor creponată.</li>
  <li>Utilizare: Scopuri adecvate, industria mobilei.</li>
  <li>Lăţime: 120 cm.</li>
  <li>Greutate: 96 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată sau albită + crud (pt industria mobilei).</li>
</ul>'), ('Dungi 1', '30', 'dungi1_small.jpg', '<ul>
  <li>Caracteristică: 70% bumbac + 30% in. Dungi orizontale.</li>
  <li>Utilizare: Perdele rustice, în combinaţie cu alte materiale pt. haie exterioare.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 257 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată.</li>
</ul>'), ('Dungi 2', '30', 'dungi2_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Feţe de masă, prosop de bucătărie, în combinaţie cu alte materiale pt. haine exterioare</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 257 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată + culori. </li>
</ul>'), ('Dungi 3', '30', 'dungi3_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Feţe de masă, prosop de bucătărie, în combinaţie cu alte materiale pt. haine exterioare</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 257 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată + culori.</li>
</ul>'), ('Dungi 4', '30', 'dungi4_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Feţe de masă, prosop de bucătărie, în combinaţie cu alte materiale pt. haine exterioare</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 257 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată + culori.</li>
</ul>'), ('Dungi 5', '30', 'dungi5_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Feţe de masă, prosop de bucătărie, în combinaţie cu alte materiale pt. haine exterioare</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 257 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată + culori.</li>
</ul>'), ('Dungi 6', '30', 'dungi6_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac.</li>
  <li>Utilizare: Feţe de masă, prosop de bucătărie, în combinaţie cu alte materiale pt. haine exterioare</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 257 g/m2.</li>
  <li>Observaţie: Comercializăm în formă spălată + culori.</li>
</ul>'), ('Rips 12', '30', 'rips12_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Dungi oblice care se intercalează sub formă de romb.</li>
  <li>Utilizare: Feţe de masă, prosoape de bucătărie, haine exterioare.</li>
  <li>Lăţime: 140 cm.</li>
  <li>Greutate: 250 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Flora 2', '30', 'flora2_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Aspect creponat. Ţesătură subţire.</li>
  <li>Utilizare: Cămăşi, bluze de vară.</li>
  <li>Lăţime: 110 cm.</li>
  <li>Greutate: 116 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Crepe 34', '30', 'crepe34_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Dungi longitudinale cu efect creponat.</li>
  <li>Utilizare: Cămăşi de vară.</li>
  <li>Lăţime: 120 cm.</li>
  <li>Greutate: 109 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Crepe 20', '30', 'crepe20_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Varianta groasă a CREPE 34.</li>
  <li>Utilizare: Cămăşi de iarnă, lenjerie de pat.</li>
  <li>Lăţime: 120 cm.</li>
  <li>Greutate: 153 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Greta 20', '30', 'greta20_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Combinaţie dungi longitudinale + dungi cu aspect dantelă din ţesere.</li>
  <li>Utilizare: Cămăşi de iarnă, lenjerie de pat.</li>
  <li>Lăţime: 130 cm.</li>
  <li>Greutate: 157 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Greta 34', '30', 'greta34_small.jpg', '<ul>
  <li>Caracteristică: Ţesătură 100 % bumbac. Combinaţie dungi longitudinale + dungi cu aspect dantelă din ţesere.</li>
  <li>Utilizare: Cămăşi de vară.</li>
  <li>Lăţime: 130 cm.</li>
  <li>Greutate: 107 g/m2.</li>
  <li>Observaţie: Comercializăm sub formă spălată şi albită.</li>
</ul>'), ('Covor', '30', 'carpet_small.jpg', '<ul>
  <li>Caracteristică: Covoare stil rustic ţesute în culori diferite.</li>
 </ul>')";

if ($conn->query($query) === TRUE) {
    echo "<script>console.log('New record created successfully');</script>";
    } else {
    echo "<script>console.log('Error');</script>";
    }

 








?>