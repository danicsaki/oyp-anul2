<!doctype html>
<html lang="en">

<head>
    <title>Kelev-Trans</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>

<body>

    <?php
        include 'db_connection.php';
    ?>

    <header class="header">
      <a href="index.php" class="header-button-left">
          <img src="https://via.placeholder.com/180x60" alt="">
      </a>

      <a class="header-button-right" id="flip">
          <i class="fas fa-bars fa-2x"></i>
      </a>

      <div id="panel" class="normal-panel">
          <div class="row">
              <div class="col text-uppercase"><a href="contact.php">Contact Us</a></div>
              <div class="col text-uppercase"><a href="despre_noi.php">About Us</a></div>
              <div class="col text-uppercase"><a href="#">Lorem Ipsum</a></div>
          </div>
      </div>

      <div id="panel" class="mobile-panel">
          <div class="row text-uppercase"><a href="contact.php">Contact Us</a></div>
          <div class="row text-uppercase"><a href="despre_noi.php">About Us</a></div>
          <div class="row text-uppercase"><a href="#">Lorem Ipsum</a></div>
      </div>
  </header>