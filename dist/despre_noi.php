<?php
include '_includes/header.php';
?>

  <div class="row">
    <div class="background-image">
      <img src="3346fb7df548990e817ab6c330b038e5.png" alt="" class="individual-background">
      <div class="individual-text">
        <h1>Despre noi</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare dui
          in
          tellus commodo, ut consequat elit pulvinar. Cras sit amet mi tincidunt, mattis purus sit
          amet,
          tempus lacus.
        </p>
        <h4>Prietenii nostrii sunt natura si materialele naturale</h4>
      </div>
    </div>
  </div>
  <div class="row about-text">
    <div class="col-6 text-section">
      <p>În mare parte producem ţesături pe bază de comandă fermă,
        dar tot timpul avem stoc şi pentru eventualii noi clienţi. Cele mai multe comenzi le primim din regiunea
        oraşului Odorheiu Secuiesc, dar avem clienţi aproape pe tot teritoriul ţării. Marfa se poate cumpăra dela firmă
        ( punct de lucru ) dar în caz de cantităţi mai mari şi la cererea clienţilor putem asigura transportul la
        destinaţie.</p>
    </div>
    <div class="col-6">
      <p>Articolele noastre sunt o varietate mare de ţesături cu proiectare şi
        dezvoltare proprie. Din punct de vedere al utilizării, putem obţine o gamă variată de greutăţi de ţesături (
        50-320 g/m2 ). Lăţimea ţesăturilor variază între 110 – 160 cm. Capacitatea firmei: 16000 m2/lună (în două
        schimburi).</p>
    </div>
  </div>
  <div class="row background-image">
    <img src="3346fb7df548990e817ab6c330b038e5.png" alt="" class="individual-background">
  </div>
  <div class="row about-text">
    <div class="col text-section">
      <p>Producem ţesături în exclusivitate din 100% bumbac sau bumbac
        în amestec cu in. În continuare ne străduim să proiectăm cât mai multe articole noi, în ultimile 7-8 ani gama
        articolelor noastre s-a ridicat la 60 de feluri de ţesături. Ne străduim să oferim clienţilor noştrii o calitate
        cât mai bună şi să câştigăm cât mai mulţi clienţi noi, dar la fel cum spune și motto-ul nostru, „Prietenii
        noștrii sunt natura și materialele naturale”, ne interesează și apărarea mediului înconjurător.</p>
    </div>
  </div>

  <?php

  include '_includes/footer.php';

  ?>