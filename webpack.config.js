const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },

  module: {
        rules: [

          {
             test: /\.css$/,
             use: [
               'style-loader',
               'css-loader',
             ],
           },

        {
             test: /\.(png|svg|jpg|gif)$/,
             use: [
               'file-loader',
             ],
           },

           {
             test: /\.(woff|woff2|eot|ttf|otf)$/,
             use: [
               'file-loader',
             ],
           },

           {
            test: /\.s[ac]ss$/i,
            use: [
              // Creates `style` nodes from JS strings
              'style-loader',
              // Translates CSS into CommonJS
              'css-loader',
              // Compiles Sass to CSS
              'sass-loader',
            ],
          },

          {
            test: /\.(html)$/,
            use: {
              loader: 'html-loader',
              options: {
                attrs: [':data-src']
              }
            }
          },

        ],
    },
};